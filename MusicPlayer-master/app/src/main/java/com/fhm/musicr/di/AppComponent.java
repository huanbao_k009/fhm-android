package com.fhm.musicr.di;

import com.fhm.musicr.ui.page.accountpage.AccountTabFragment;
import com.fhm.musicr.ui.page.forgetpass.ForgetPassFragment;
import com.fhm.musicr.ui.page.profilepage.ProfileFragment;
import com.fhm.musicr.ui.page.signup.SignUpFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})

public interface  AppComponent {//Khai báo Fragment gọi API
    void inject(AccountTabFragment accountTabFragment);
    void inject(ProfileFragment profileFragment);
    void inject(ForgetPassFragment forgetPassFragment);
    void inject(SignUpFragment signUpFragment);
}
