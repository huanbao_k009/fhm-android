package com.fhm.musicr.helper.menu;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.fhm.musicr.R;
import com.fhm.musicr.model.Song;
import com.fhm.musicr.service.MusicPlayerRemote;
import com.fhm.musicr.ui.MainActivity;
import com.fhm.musicr.ui.bottomsheet.LyricBottomSheet;
import com.fhm.musicr.ui.dialog.AddToPlaylistDialog;
import com.fhm.musicr.ui.dialog.DeleteSongsDialog;
import com.fhm.musicr.util.MusicUtil;
import com.fhm.musicr.util.NavigationUtil;
import com.fhm.musicr.util.RingtoneManager;

/**
 * @author Karim Abou Zeid (kabouzeid)
 *
 */
public class SongMenuHelper {
    @StringRes
    public static final int[] SONG_OPTION = new int[]{
            /*   R.string.play,*/
            R.string.play_next,

            R.string.add_to_queue,
            R.string.add_to_playlist,
            /*    R.string.go_to_source_playlist,*/
            /*        R.string.go_to_album,*/
            R.string.go_to_artist,
            R.string.show_lyric,
            R.string.divider,
            R.string.share,
            R.string.set_as_ringtone,
            /*  R.string.delete_from_playlist,*/
            R.string.delete_from_device
    };

    @StringRes
    public static final int[] SONG_QUEUE_OPTION = new int[]{
            R.string.play_next,
            R.string.play_preview,
            R.string.remove_from_queue,
            R.string.add_to_playlist,
            /* R.string.go_to_source_playlist,*/
            /*        R.string.go_to_album,*/
            R.string.go_to_artist,
            R.string.show_lyric,
            R.string.divider,
            R.string.share,
            R.string.set_as_ringtone,
            /*  R.string.delete_from_playlist,*/
            R.string.delete_from_device
    };

    @StringRes
    public static final int[] NOW_PLAYING_OPTION = new int[]{
            R.string.repeat_it_again,
            R.string.play_preview,
            /* R.string.remove_from_queue,*/
            /*  R.string.go_to_source_playlist,*/
            R.string.add_to_playlist,
            /*        R.string.go_to_album,*/
            R.string.go_to_artist,
            R.string.show_lyric,
            R.string.divider,
            R.string.share,
            R.string.set_as_ringtone,
            /*  R.string.delete_from_playlist,*/
            R.string.delete_from_device
    };

    @StringRes
    public static final int[] SONG_ARTIST_OPTION= new int[]{
            /*   R.string.play,*/
            R.string.play_next,
            R.string.play_preview,
            R.string.add_to_queue,
            R.string.add_to_playlist,
            /*    R.string.go_to_source_playlist,*/
            /*        R.string.go_to_album,*/
            /*R.string.go_to_artist,*/
            R.string.show_lyric,
            R.string.divider,
            R.string.share,
            R.string.set_as_ringtone,
            /*  R.string.delete_from_playlist,*/
            R.string.delete_from_device
    };

    public static boolean handleMenuClick(@NonNull AppCompatActivity activity, @NonNull Song song, int string_res_option) {
        switch (string_res_option) {
            case R.string.play_preview:
                if(activity instanceof MainActivity) {
                    ((MainActivity)activity).getSongPreviewController().previewSongs(song);
                }
                break;
            case R.string.set_as_ringtone:
                if (RingtoneManager.requiresDialog(activity)) {
                    RingtoneManager.showDialog(activity);
                } else {
                    RingtoneManager ringtoneManager = new RingtoneManager();
                    ringtoneManager.setRingtone(activity, song.id);
                }
                return true;
            case R.string.share:
                activity.startActivity(Intent.createChooser(MusicUtil.createShareSongFileIntent(song, activity), null));
                return true;
            case R.string.delete_from_device:
                DeleteSongsDialog.create(song).show(activity.getSupportFragmentManager(), "DELETE_SONGS");
                return true;
            case R.string.add_to_playlist:
                AddToPlaylistDialog.create(song).show(activity.getSupportFragmentManager(), "ADD_PLAYLIST");
                return true;
            case R.string.repeat_it_again:
            case R.string.play_next:
                MusicPlayerRemote.playNext(song);
                return true;
            case R.string.add_to_queue:
                MusicPlayerRemote.enqueue(song);
                return true;
            case R.string.show_lyric:
                LyricBottomSheet.newInstance(song).show(activity.getSupportFragmentManager(),LyricBottomSheet.TAG);
                break;
            case R.string.go_to_album:
                // NavigationUtil.goToAlbum(activity, song.albumId);
                return true;
            case R.string.go_to_artist:
                NavigationUtil.navigateToArtist(activity, song.artistId);
                return true;
        }
        return false;
    }


}
