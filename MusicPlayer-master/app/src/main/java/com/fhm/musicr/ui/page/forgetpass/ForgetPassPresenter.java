package com.fhm.musicr.ui.page.forgetpass;

import android.util.Log;

import com.fhm.musicr.base.MvpBasePresenter;
import com.fhm.musicr.datasource.models.ForgetPassRequest;
import com.fhm.musicr.datasource.repository.AppRepository;

import java.net.SocketTimeoutException;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ForgetPassPresenter extends MvpBasePresenter<ForgetPassContract.View> implements  ForgetPassContract.Presenter {

    private AppRepository mAppRepository;

    @Inject
    public ForgetPassPresenter(AppRepository appRepository) {
        this.mAppRepository = appRepository;
    }

    //Cấu hình API
    @Override
    public void forgotPass(ForgetPassRequest forgetPassRequest) {
        System.out.println("forgotPass Presenter");
        getView().showLoading();
        addSubscribe(mAppRepository.forgotPass(forgetPassRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    getView().hideLoading();
                    Log.d("MIEN-SUCCESS", r.getData().toString());
                }, e -> {
                    Log.d("MIEN-FAILS", e.getMessage());
                    getView().hideLoading();
                    if (e instanceof SocketTimeoutException) {
                        getView().showTimeoutConnection();
                    } else {
                        getView().showError(parseError(e).getErrorCode());
                    }
                }));
    }
}
