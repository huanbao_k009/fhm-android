package com.fhm.musicr.base;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.fhm.musicr.ui.widget.fragmentnavigationcontroller.SupportFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends SupportFragment {

//    protected abstract int getResLayoutId();

    private Unbinder unbinder;
//    protected OnFragmentInteractionListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String title);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (isVisible()) {
                    stateChange();
                }
            }
        });
    }

    public void loadData() {
    }

    protected void stateChange() {
    }

//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        if (getUserVisibleHint()) {
//            loadData();
//        }
//        return inflater.inflate(getResLayoutId(), container, false);
//    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        injectComponent();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            loadData();
        }
    }


    protected void injectComponent() {
        //No action
    }

    protected void onPreDestroyView() {

    }

    @Override
    public void onDestroyView() {
        onPreDestroyView();
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mListener = null;
    }

    protected void goBack() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    protected void showProgressDialog() {
        if (getFragmentManager() != null) {
            LoadingDialogFragment.show(getFragmentManager());
        }
    }

    protected void hideProgressDialog() {
        if (getFragmentManager() != null) {
            LoadingDialogFragment.hide(getFragmentManager());
        }
    }
}
