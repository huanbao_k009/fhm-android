package com.fhm.musicr.ui.page.profilepage;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fhm.musicr.App;
import com.fhm.musicr.R;
import com.fhm.musicr.base.MvpBaseFragment;
import com.fhm.musicr.datasource.models.UserProfileRequest;
import com.fhm.musicr.ui.widget.rangeseekbar.OnRangeChangedListener;
import com.fhm.musicr.ui.widget.rangeseekbar.RangeSeekBar;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends MvpBaseFragment<ProfilePresenter> implements OnRangeChangedListener, ProfileContract.View  {

    @BindView(R.id.btn_logout)
    Button btnLogout;

    @BindView(R.id.btn_update)
    Button btnUpdate;

    @BindView(R.id.editTxtPassword_Update)
    EditText txtPassword;

    @BindView(R.id.editTxtConfirmPassword_Update)
    EditText txtConfirmPassword;

    @BindView(R.id.editTxtPersonName_Profile)
    EditText txtName;

    @BindView(R.id.imageView_avatar)
    ImageView Avatar;

    @BindView(R.id.btn_edit_ava)
    Button btnEditAva;

    @BindView(R.id. btn_save_ava)
    Button btnSaveAva;

    int Request_Code_Image = 0;
    String img_path = "";
    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instanc  e of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent().inject(this);
    }

    @Nullable
    @Override
    protected View onCreateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        ImageView avatar = getView().findViewById(R.id.imageView_avatar);
        if (avatar == null) Log.d("test", "Null");
        Picasso.get().load(App.getInstance().getPreferencesUtility().getUserAvatar()).into(avatar);
        txtName.setText(App.getInstance().getPreferencesUtility().getUserName());
        getPresenter().getSongs();
        onClick();
    }
    private void onClick() {
        btnLogout.setOnClickListener(v -> {
            getPresenter().logout();
        });
        btnUpdate.setOnClickListener(v -> {
//            if (txtPassword.getText().toString().trim() == txtConfirmPassword.getText().toString().trim())
            getPresenter().updateProfile(new UserProfileRequest("normal", App.getInstance().getPreferencesUtility().getUserEmail(), txtPassword.getText().toString().trim(), txtName.getText().toString().trim()));
        });
        btnSaveAva.setOnClickListener(v -> {
            try {
            if (img_path != ""){
                File file = new File(img_path);
                String file_path = file.getAbsolutePath();
                String[] arr_file_path = file_path.split("\\.");
                file_path = App.getInstance().getPreferencesUtility().getUserId() + "." + arr_file_path[1];
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("fileImage", file_path, requestBody);
                getPresenter().uploadImage(body);
            }
            }catch (Exception e){
                Log.e("err",e.getMessage());
            }
        });
        btnEditAva.setOnClickListener(v ->{
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, Request_Code_Image);
        });
    }
    public String getRealPathFromURI (Uri contentUri) {
        String path = null;
        String[] proj = { MediaStore.MediaColumns.DATA };
        Cursor cursor =  getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == Request_Code_Image && resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();
            img_path = getRealPathFromURI(uri);
            try {
                InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                Avatar.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
            super.onActivityResult(requestCode, requestCode, data);
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {

    }

    @Override
    public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

    }

    @Override
    public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

    }

    @Override
    public void navigateMainScreen(Object data) {
        App.getInstance().getPreferencesUtility().setSONGS(data);
    }
}