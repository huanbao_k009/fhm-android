package com.fhm.musicr.base;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import javax.inject.Inject;

public abstract class MvpBaseFragment<P extends MvpBasePresenter> extends BaseFragment implements MvpView {
    @Inject
    P presenter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (presenter != null) {
            presenter.onAttachView(this);
        }
    }


    @Override
    public void showLoading() {
        showProgressDialog();
    }

    @Override
    public void hideLoading() {
        hideProgressDialog();
    }

    @Override
    protected void onPreDestroyView() {
        hideLoading();
        if (presenter != null) {
            presenter.onDetachView();
        }
    }

    protected P getPresenter() {
        return presenter;
    }

    @Override
    public void showError(int resString) {
        showError(getString(resString));
    }

    @Override
    public void showNoInternetConnection() {
        Toast.makeText(getActivity(), "Vui lòng kiểm tra kết nối mạng và thử lại", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showTimeoutConnection() {
        Toast.makeText(getActivity(), "Quá thời hạn kết nối tới máy chủ, vui lòng thử lại", Toast.LENGTH_SHORT).show();
    }
}

