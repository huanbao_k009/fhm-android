package com.fhm.musicr.ui.page.accountpage;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fhm.musicr.App;
import com.fhm.musicr.R;
import com.fhm.musicr.base.MvpBaseFragment;
import com.fhm.musicr.datasource.models.LoginRequest;
import com.fhm.musicr.ui.page.forgetpass.ForgetPassFragment;
import com.fhm.musicr.ui.page.profilepage.ProfileFragment;
import com.fhm.musicr.ui.page.signup.SignUpFragment;
import com.fhm.musicr.ui.widget.navigate.NavigateFragment;
import com.fhm.musicr.ui.widget.rangeseekbar.OnRangeChangedListener;
import com.fhm.musicr.ui.widget.rangeseekbar.RangeSeekBar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountTabFragment extends MvpBaseFragment<AccountPresenter> implements OnRangeChangedListener,AccountContract.View {

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.btn_sign_up)
    Button btnSignUp;

    @BindView(R.id.editTextTextEmailAddress)
    EditText txtEmailAddress;

    @BindView(R.id.editTextTextPassword)
    EditText txtPassword;

    @BindView(R.id.btn_forget_pass)
    Button btnForgetPass;

    public static final String emailPattern = "[a-zA-Z0-9._-]+@[a-z.-]+\\.+[a-z]{2,4}$/";
    private Boolean isValidEmail = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent().inject(this);
    }

    @Nullable
    @Override
    protected View onCreateView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.account_tab_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        String mCurrentToken = App.getInstance().getPreferencesUtility().getToken();
        if(mCurrentToken == "") onClick();
        else navigateMainScreen();
    }
    //Gọi API login
    private void onClick() {
        btnLogin.setOnClickListener(v -> {
            System.out.println(txtEmailAddress.getText().toString().trim());
            getPresenter().login(new LoginRequest("normal", txtEmailAddress.getText().toString().trim(), txtPassword.getText().toString().trim()));
        });
        btnSignUp.setOnClickListener(v -> {
            SignUpScreen();
//            getPresenter().logout();
        });
        btnForgetPass.setOnClickListener(v -> {
            ForgetPassScreen();
           // getPresenter().forgotPass(new ForgetPassRequest("normal", txtEmailAddress.getText().toString().trim()));
        });


    }

    private void SignUpScreen() {
        MvpBaseFragment sf = SignUpFragment.newInstance();
        Fragment parentFragment = getParentFragment();
        assert parentFragment != null;
        ((NavigateFragment)parentFragment).presentFragment(sf);
    }


    @Override
    public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {

    }

    @Override
    public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

    }

    @Override
    public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

    }

    public void ForgetPassScreen() {
        MvpBaseFragment sf = ForgetPassFragment.newInstance();
        Fragment parentFragment = getParentFragment();
        assert parentFragment != null;
        ((NavigateFragment)parentFragment).presentFragment(sf);
    }

    @Override
    public void navigateMainScreen() {
        MvpBaseFragment sf = ProfileFragment.newInstance();
        Fragment parentFragment = getParentFragment();
        assert parentFragment != null;
        ((NavigateFragment)parentFragment).presentFragment(sf);
    }

    @Override
    public void showError(String message) {

    }
}
