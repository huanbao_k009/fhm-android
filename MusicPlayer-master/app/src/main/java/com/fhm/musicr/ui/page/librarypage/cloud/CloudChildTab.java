package com.fhm.musicr.ui.page.librarypage.cloud;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fhm.musicr.App;
import com.fhm.musicr.R;
import com.fhm.musicr.contract.AbsMediaAdapter;
import com.fhm.musicr.loader.medialoader.SongLoader;
import com.fhm.musicr.model.Song;
import com.fhm.musicr.ui.bottomsheet.SortOrderBottomSheet;
import com.fhm.musicr.ui.page.BaseMusicServiceFragment;
import com.fhm.musicr.ui.page.librarypage.song.PreviewRandomPlayAdapter;
import com.fhm.musicr.ui.page.librarypage.song.SongSearchEvent;
import com.fhm.musicr.util.Animation;
import com.fhm.musicr.util.Tool;
import com.fhm.musicr.util.Util;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CloudChildTab extends BaseMusicServiceFragment implements SortOrderBottomSheet.SortOrderChangedListener, PreviewRandomPlayAdapter.FirstItemCallBack{
    public static final String TAG ="SongChildTab";

//    public static SongChildTab songChildTab = null;
    CloudChildAdapter mAdapter;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

//    @BindView(R.id.preview_shuffle_list)
//    RecyclerView mPreviewRecyclerView;

    @BindView(R.id.refresh)
    ImageView mRefresh;

    @BindView(R.id.image)
    ImageView mImage;

    @BindView(R.id.title)
    TextView mTitle;

    @BindView(R.id.description)
    TextView mArtist;

    @BindView(R.id.random_group)
    Group mRandomGroup;

    private int mCurrentSortOrder = 0;
    private void initSortOrder() {
         mCurrentSortOrder = App.getInstance().getPreferencesUtility().getCloudChildSortOrder();
    }


    // ---- Khai bao dang ky nhan su kien dung EventBus --- //
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    // ---- Khai bao huy dang ky nhan su kien dung EventBus --- //
    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    // Ham nhan su kien EventBus
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SongSearchEvent event) {
        // Du lieu nhan duoc tu LibraryTabFragment
        // Goi ham searchMySong de tim kiem bai hat va load lai giao dien
//        searchMySong(event.getMessage());
    };

    // Tim kiem bai hat va load lai giao dien
    public void searchMySong( String songName){
        ArrayList<Song> songs = SongLoader.getSongs(getActivity(), songName);

        mAdapter.setData(songs);
        showOrHidePreview(!songs.isEmpty());
    }

    @OnClick({R.id.preview_random_panel})
     void shuffle() {
        mAdapter.shuffle();
    }


//    PreviewRandomPlayAdapter mPreviewAdapter;

    @OnClick(R.id.refresh)
    void refresh() {
        mRefresh.animate().rotationBy(360).setInterpolator(Animation.getInterpolator(6)).setDuration(650);
        mRefresh.postDelayed(mAdapter::randomize,300);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.song_child_tab,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        initSortOrder();

        mAdapter = new CloudChildAdapter(getActivity());
        mAdapter.setName(TAG);
        mAdapter.setCallBack(this);
        mAdapter.setSortOrderChangedListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        mRecyclerView.setAdapter(mAdapter);

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("Delay:","alo");
                refreshData();
            }
        }, 1000);
    }

    @Override
    public void onDestroyView() {
        mAdapter.destroy();
        super.onDestroyView();
    }

    private void refreshData() {
        ArrayList<Song> songs = new ArrayList<Song>();
        String mCurrentToken = App.getInstance().getPreferencesUtility().getToken();
        Object cloud_songs = App.getInstance().getPreferencesUtility().getSONGS();
        int id = 0;
        if(mCurrentToken != "" && cloud_songs != null) {
            for (Object cloud_song : ((ArrayList) cloud_songs)) {
                Map<String, Object> test = (Map<String, Object>) cloud_song;
                id = id + (int)1;
                Song song =new Song(id, (String)test.get("title"), (int)Math.round((double)test.get("trackNumber")) , 2020, (int)Math.round((double)test.get("duration")), (String) test.get("data"), (int)Math.round((double)test.get("dateModified")), 0, (String) test.get("albumName"), 0, (String)test.get("artistName"), (String)test.get("albumArtPath"), (int)Math.round((double)test.get("playCount")));
                songs.add(song);
            }
        }
        if ((mCurrentSortOrder % 3) == 0)
            Collections.sort(songs, new Comparator<Song>() {
                   @Override
                   public int compare(Song ls, Song rs) {
                       if (mCurrentSortOrder < 3) return ls.getName().compareTo(rs.getName());
                       else return rs.getName().compareTo(ls.getName());
                   }
            });
        else if ((mCurrentSortOrder % 3) == 1)
            Collections.sort(songs, new Comparator<Song>() {
                @Override
                public int compare(Song ls, Song rs) {
                    if (mCurrentSortOrder < 3) return ls.getDateModified().compareTo(rs.getDateModified());
                    else return rs.getDateModified().compareTo(ls.getDateModified());
                }
            });
        else if ((mCurrentSortOrder % 3) == 2)
            Collections.sort(songs, new Comparator<Song>() {
                @Override
                public int compare(Song ls, Song rs) {
                    if (mCurrentSortOrder < 3) return rs.getPlayCount().compareTo(ls.getPlayCount());
                    else return ls.getPlayCount().compareTo(rs.getPlayCount());
                }
            });

        if (songs.size() != 0 ) mAdapter.setData(songs);
        showOrHidePreview(false);
    }
    private void showOrHidePreview(boolean show) {
        int v = show ? View.VISIBLE : View.GONE;
        mRandomGroup.setVisibility(v);
    }

    @Override
    public void onFirstItemCreated(Song song) {
        mTitle.setText(song.title);
        mArtist.setText(song.artistName);

        Glide.with(this)
                .load(Util.getAlbumArtUri(song.albumId))
                .placeholder(R.drawable.music_style)
                .error(R.drawable.music_empty)
                .into(mImage);

    }

    @Override
    public void onPlayingMetaChanged() {
        if(mAdapter!=null)mAdapter.notifyOnMediaStateChanged(AbsMediaAdapter.PLAY_STATE_CHANGED);
    }

    @Override
    public void onPaletteChanged() {
        if(mRecyclerView instanceof FastScrollRecyclerView) {
            FastScrollRecyclerView recyclerView = ((FastScrollRecyclerView)mRecyclerView);
            recyclerView.setPopupBgColor(Tool.getHeavyColor());
            recyclerView.setThumbColor(Tool.getHeavyColor());
        }
        mAdapter.notifyOnMediaStateChanged(AbsMediaAdapter.PALETTE_CHANGED);
        super.onPaletteChanged();
    }

    @Override
    public void onPlayStateChanged() {
        if(mAdapter!=null)mAdapter.notifyOnMediaStateChanged(AbsMediaAdapter.PLAY_STATE_CHANGED);
    }

    @Override
    public void onMediaStoreChanged() {
        ArrayList<Song> songs = SongLoader.getAllSongs(getActivity(),SortOrderBottomSheet.mSortOrderCodes[mCurrentSortOrder]);
        mAdapter.setData(songs);
        showOrHidePreview(!songs.isEmpty());
    }

    @Override
    public int getSavedOrder() {
        return mCurrentSortOrder;
    }

    @Override
    public void onOrderChanged(int newType, String name) {
        if(mCurrentSortOrder!=newType) {
            mCurrentSortOrder = newType;
            Log.d("Sort", String.valueOf(newType));
            App.getInstance().getPreferencesUtility().setCloudChildSortOrder(mCurrentSortOrder);
            refreshData();
        }
    }
}
