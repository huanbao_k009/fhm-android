package com.fhm.musicr.datasource.repository;

import com.fhm.musicr.datasource.models.BaseResponse;
import com.fhm.musicr.datasource.models.ForgetPassRequest;
import com.fhm.musicr.datasource.models.LoginRequest;
import com.fhm.musicr.datasource.models.LoginResponse;
import com.fhm.musicr.datasource.models.UserProfileRequest;
import com.fhm.musicr.datasource.network.ApiService;

import io.reactivex.Single;
import okhttp3.MultipartBody;

public class AppRepositoryImpl implements AppRepository {
    private ApiService apiService;

    public AppRepositoryImpl(ApiService apiService) {
        this.apiService = apiService;
    }

//Overide lại bên AppRepository
    @Override
    public Single<BaseResponse<LoginResponse>> login(LoginRequest body) {
        System.out.println("login");
        return apiService.login(body);
    }

    @Override
    public Single<BaseResponse> signUp(UserProfileRequest body) {
        System.out.println("signUp");
        return apiService.signUp(body);
    }

    @Override
    public Single<BaseResponse> forgotPass(ForgetPassRequest body) {
        System.out.println("forgotPass");
        return apiService.forgotPass(body);
    }

    @Override
    public Single<BaseResponse> logout(String token) {
        System.out.println("logout");
        return apiService.logout(token);
    }

    @Override
    public Single<BaseResponse> updateProfile(String token, UserProfileRequest body) {
        System.out.println("updateProfile");
        return apiService.updateProfile(token, body);
    }

    @Override
    public Single<BaseResponse> uploadImage(String token, MultipartBody.Part body) {
        return apiService.uploadImage(token, body);
    }

    @Override
    public Single<BaseResponse> getSongs(String token) {
        System.out.println("get songs");
        return apiService.getSongs(token);
    }


}
