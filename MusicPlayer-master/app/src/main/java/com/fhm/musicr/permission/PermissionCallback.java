package com.fhm.musicr.permission;

/**
 * Created by huan on 0005 05 Feb 2018.
 */

public interface PermissionCallback {
    void permissionGranted();

    void permissionRefused();
}
